package com.demo.tictactoe;

import java.util.Random;

public class Player {

    private ActionFigure figure;

    public Player(ActionFigure figure) {
        this.figure = figure;
    }

    public Move turn(Board board) {
        Move move = new Move();
        move.setFigure(figure);

        FigurePosition pos = selectPosition(board);

        move.setX(pos.getX());
        move.setY(pos.getY());

        return move;
    }

    private FigurePosition selectPosition(Board board) {
        final Random rnd = new Random();

        int field;
        do {
            field = rnd.nextInt(board.size() - 1);
        } while (!board.fieldIsEmpty(field) && board.hasMoreSpace());

        return board.getPosition(field);
    }

}
