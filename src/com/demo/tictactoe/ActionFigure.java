package com.demo.tictactoe;

public enum ActionFigure {
	//нулик
	NOUGHT{
		public String toString(){
			return "O";
		}
	},
	//хрестик
	CROSS{
		public String toString(){
			return "X";
		}
	}
}
