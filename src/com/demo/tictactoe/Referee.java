package com.demo.tictactoe;

public class Referee {

    // available vectors on board
    private int[][] boardVectors = {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8},
            {0, 3, 6},
            {1, 4, 7},
            {2, 5, 8},
            {0, 4, 8},
            {2, 4, 6}
    };

    public void put(Move move, Board board) {
        board.putFigureToBoard(new FigurePosition(move.getX(), move.getY()), move.getFigure());
    }

    public boolean isWin(Move move, Board board) {

        int field = move.getX() * board.getSizeX() + move.getY();

        for (int i = 0; i < boardVectors.length; i++) {
            if (!isFieldInVector(field, boardVectors[i]))
                continue;

            if (isVectorWin(boardVectors[i], board))
                return true;
        }

        return false;
    }

    private boolean isFieldInVector(int field, int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == field)
                return true;
        }
        return false;
    }

    private boolean isVectorWin(int[] vector, Board board) {
        boolean compareResult = true;

        ActionFigure actionFigure = board.getFigureFromBoardField(vector[0]);

        for (int i = 1; i < vector.length; i++) {
            if(!(actionFigure == board.getFigureFromBoardField(vector[i])))
                compareResult = false;
        }
        return compareResult;
    }

}
