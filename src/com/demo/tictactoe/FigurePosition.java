package com.demo.tictactoe;

public class FigurePosition {

    private int x;
    private int y;

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    FigurePosition() {
    }

    FigurePosition(int x, int y) {
        setX(x);
        setY(y);
    }
}
