package com.demo.tictactoe;

import java.util.LinkedList;

public class TheGame {

    public static void main(String[] args) {
        Referee referee = new Referee();

        Player player1 = new Player(ActionFigure.NOUGHT);
        Player player2 = new Player(ActionFigure.CROSS);

        Player[] players = {player1, player2};

        Board board = new Board();
        Move move;

        win:
        while (board.hasMoreSpace()) {
            for (int i = 0; i < players.length; i++) {
                move = players[i].turn(board);
                referee.put(move, board);
                board.print();
                if (referee.isWin(move, board)) {
//                    System.out.println("You WIN!!!");
                    break win;
                }
            }
        }
    }

}
