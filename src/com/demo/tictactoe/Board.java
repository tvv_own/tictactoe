package com.demo.tictactoe;

public class Board {
    private int sizeX;
    private int sizeY;

    private ActionFigure[][] board;

    private int moreSpaceCount;

    private final int freeSpaceLimit = 2;

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int size() {
        return sizeX * sizeY;
    }

    Board() {
        sizeX = 3;
        sizeY = 3;
        board = new ActionFigure[sizeX][sizeY];
        moreSpaceCount = sizeX * sizeY;
    }

    public boolean hasMoreSpace() {
        return moreSpaceCount >= freeSpaceLimit;
//        if (moreSpaceCount >= freeSpaceLimit)
//            return true;
//
//        for (int i = 0; i < sizeX; i++) {
//            for (int j = 0; j < sizeY; j++) {
//                if (board[i][j] != null)
//                    continue;
//                if (++moreSpaceCount >= freeSpaceLimit)
//                    return true;
//            }
//        }
//        return false;
    }

    public void print() {
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                System.out.print(board[i][j] == null ? " " : board[i][j]);
                if (j < sizeY - 1)
                    System.out.print(" | ");
            }
            System.out.println();
            if (i < sizeX - 1) {
                for (int k = 0; k < (sizeX * 3); k++) {
                    System.out.print("-");
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    public boolean fieldIsEmpty(int searchField) {
        return board[searchField / sizeX][searchField % sizeX] == null;
    }

    public FigurePosition getPosition(int searchField){
        return new FigurePosition(searchField / sizeX, searchField % sizeX);
    }

    public void putFigureToBoard(FigurePosition pos, ActionFigure figure){
        board[pos.getX()][pos.getY()] = figure;
        moreSpaceCount--;
    }

    public ActionFigure getFigureFromBoardField(int field){
        FigurePosition pos = getPosition(field);
        return board[pos.getX()][pos.getY()];
    }
}
